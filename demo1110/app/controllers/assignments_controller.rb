class AssignmentsController < ApplicationController
  def new
    @assignment = Assignment.new
    @course = Course.find(params[:course_id])
    @type = params[:assign_type]
    #khan courses private variable
    if @type == "khan"
      @khan = JSON.parse(File.read("#{Rails.root}/public/khan-exercises.json"))
      @khan_exercises = Array.new
      @khan.each do |k|
        @khan_exercises += [k["display_name"]]
      end
    end
  end

  def edit
    @assignment = Assignment.find(params[:id])
  end

  def index
    @assignment = Assignment.all
  end

  def show
    @assignment = Assignment.find(params[:id])
    @course = @assignment.course
  end

  def create
    type = params[:assign_type]
    khan = JSON.parse(File.read("#{Rails.root}/public/khan-exercises.json"))
    @course = Course.find(params[:course_id])
    students = @course.users.where("role = 'Student'")

    @assignment = Assignment.new(assignment_params)

    khan_exercise = nil
    if type == "khan"
      khan.each do |k|
        khan_exercise = k if k["display_name"] == @assignment.khan_exercise
      end
      @assignment.name = khan_exercise["pretty_display_name"]
      @assignment.khan_exercise = khan_exercise["ka_url"]
      @assignment.description = khan_exercise["description"]
    end
    # params.require(:assignment).permit(:name, :khan_exercise, :description, :due_date)

    if @assignment.save
      students.each do |s|
        s.submissions.create(assignment: @assignment, status: "Incomplete", image: File.new("#{Rails.root}/public/empty.jpg"))
      end
      @course.assignments << @assignment
      redirect_to @assignment
    else
      render 'new'
    end
  end

  def update

    @assignment = Assignment.find(params[:id])

    if @assignment.update(assignment_params)
      redirect_to @assignment
    else
      render 'edit'
    end
  end

  def destroy

    @assignment = Assignment.find(params[:id])
    @assignment.destroy

    redirect_to assignments_path
  end



  private
  def assignment_params
    params.require(:assignment).permit(:name, :khan_exercise, :description, :due_date)
  end



end
