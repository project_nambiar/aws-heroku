class SubmissionsController < ApplicationController

  def edit
    @assignment = Assignment.find(params[:assignment_id])
    @submission = Submission.new
  end

  def update
    @assignment = Assignment.find(params[:assignment_id])
    @student = current_user
    @student.submissions.each do |s|
      if s.assignment.id == @assignment.id
        s.update_attributes(submission_params)
        redirect_to @assignment
      end
    end
  end

  def show
    @submission = Submission.find(params[:submission_id])
  end

  private
  def submission_params
    params.require(:submission).permit(:status, :image)
  end
end
