require 'test_helper'

class AssignmentTest < ActiveSupport::TestCase

  test "ensure a new Assignment object is an instance of the assignment model" do
    assignment = Assignment.create(name: "Assignment test1",
                                   description: "This is the description",
                                   due_date: DateTime.new(2027))
    assert_instance_of(Assignment, assignment, msg="not a Assignment object")
  end

  test "ensure that a new Assignment object with data is not nil" do
    assignment = Assignment.create(name: "Assignment test2",
                                   description: "This is the description",
                                   due_date: DateTime.new(2027))
    assert_not_nil(assignment, msg="not a nil object")
  end

  test "ensure that important Assignment fields are not empty" do
    assignment = Assignment.create(name: "assignment test3",
                                   due_date: DateTime.new(2027))
    assert_not_nil(assignment.name, msg="name empty")
    assert_not_nil(assignment.due_date, msg="due date empty")
  end

  test "ensure a new assignment name is stored correctly" do
    assignment = Assignment.create(name: "Assignment test4",
                                   description: "this is the description",
                                   due_date: DateTime.new(2027))
    assert_equal("Assignment test4", assignment.name, msg="incorrect Assignment name")
  end

  test "ensure a new assignment description is stored correctly" do
    assignment = Assignment.create(name: "Assignment test4",
                                   description: "this is the description",
                                   due_date: DateTime.new(2027))
    assert_equal("this is the description", assignment.description, msg="incorrect Assignment description")
  end


  test "ensure a new assignment due_date is stored correctly" do
    assignment = Assignment.create(name: "Assignment test5",
                                   description: "this is the description",
                                   due_date: DateTime.new(2027))
    assert_equal(DateTime.new(2027), assignment.due_date, msg="incorrect Assignment due date")
  end

end

