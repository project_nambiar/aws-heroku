
class SubmissionTest < ActiveSupport::TestCase

  test "ensure that a User can have an Assignment" do
    user = User.create(username: "judykay", first_name: "Judy", last_name: "Kay", email: "judy@mail.com", password: "judykay1", password_confirmation: "judykay1")
    phys_assign = Assignment.create(name: "Physics 101", description: "An introductory course for Physics", due_date: DateTime.now)
    user.submissions.create(assignment: phys_assign, status: "Incomplete", image: nil)
    assert_instance_of(Submission, user.submissions.first, msg="relationship not there")
  end

  test "ensure that a Assingment can have an User" do
    user = User.create(username: "judykay", first_name: "Judy", last_name: "Kay", email: "judy@mail.com", password: "judykay1", password_confirmation: "judykay1")
    phys_assign = Assignment.create(name: "Physics 101", description: "An introductory course for Physics", due_date: DateTime.now)
    phys_assign.submissions.create(user: user, status: "Incomplete", image: nil)
    assert_instance_of(Submission, phys_assign.submissions.first, msg="relationship not there")
  end
end
