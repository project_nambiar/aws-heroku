require 'test_helper'

class CourseStudentsControllerTest < ActionController::TestCase
  test "should get show_students" do
    user = User.create(username: "judykay", first_name: "Judy", last_name: "Kay", email: "judy@mail.com", password: "judykay1", password_confirmation: "judykay1", role: "Teacher")
    course = Course.create(code: "PHYS0101", name: "Physics 101", description: "An introductory course for Physics")
    session[:user_id] = user.id
    get :show_students, :course_id => course.id
    assert_response :success
  end

  test "should get add_students" do
    @request.env['HTTP_REFERER'] = 'http://test.host/course_students/show_students?course_id=1'
    user = User.create(username: "judykay", first_name: "Judy", last_name: "Kay", email: "judy@mail.com", password: "judykay1", password_confirmation: "judykay1", role: "Teacher")
    course = Course.create(code: "PHYS0101", name: "Physics 101", description: "An introductory course for Physics")
    session[:user_id] = user.id
    get :add_students, :course_id => course.id, :student_id => user.id
    assert_redirected_to :back
  end

  test "should get remove_students" do
    @request.env['HTTP_REFERER'] = 'http://test.host/course_students/show_students?course_id=1'
    user = User.create(username: "judykay", first_name: "Judy", last_name: "Kay", email: "judy@mail.com", password: "judykay1", password_confirmation: "judykay1", role: "Teacher")
    course = Course.create(code: "PHYS0101", name: "Physics 101", description: "An introductory course for Physics")
    session[:user_id] = user.id
    get :remove_students, :course_id => course.id, :student_id => user.id
    assert_redirected_to :back
  end

end
