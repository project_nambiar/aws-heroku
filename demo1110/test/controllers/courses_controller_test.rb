require 'test_helper'

class CoursesControllerTest < ActionController::TestCase
  test "should not get index if no current user exists" do
    get :index
    assert_response :redirect
    assert_redirected_to "/login"
  end

  test "should not get new if no current user exists" do
    get :new
    assert_response :redirect
    assert_redirected_to "/login"
  end
end




