Rails.application.routes.draw do
  resources :courses do
	resources :assignments
  end

  resources :users
  resources :assignments

  root "users#show_courses"

  get "signup" => "users#new"
  post "signup" => "users#create"

  get 'login' => 'sessions#new'
  post 'login' => 'sessions#create'

  delete 'logout' => 'sessions#destroy'

  delete 'courses/:id' => 'courses#destroy'

  get 'edit' => 'users#edit'
  patch 'edit' => 'users#update'
  delete 'delete' => 'users#destroy'

  get 'assignment' => 'assignments#new'
  post 'assignment' => 'assignments#create'
  patch 'edit' => 'assignments#update'
  delete 'assignment' => 'assignments#destroy'

  get 'course_students/show_students'
  get 'course_students/add_students'
  get 'course_students/remove_students'
  get 'course_students/new_course'
  post 'course_students/create_course'

  get 'submission' => 'submissions#edit'
  post 'submission' => 'submissions#update'
  get 'submission/:id' => 'submissions#show'

end
