class CreateAssignments < ActiveRecord::Migration
  def change
    create_table :assignments do |t|
      t.belongs_to :course
      t.string :name
      t.string :khan_exercise
      t.text :description
      t.datetime :due_date

      t.timestamps null: false
    end
  end
end
