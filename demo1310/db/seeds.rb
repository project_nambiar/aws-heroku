# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or newd alongside the db with db:setup).
#
# Examples:
#
#   cities = City.new([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.new(name: 'Emanuel', city: cities.first)
#

course = Course.create(code: "Math101", name: "Statistics", description: "Learn some math")

assignment1 = Assignment.create(name: "Addition", description: "Adding is fun", due_date: DateTime.now, image: File.new("#{Rails.root}/public/empty.jpg"))
assignment2 = Assignment.create(name: "Subtraction", description: "Subtracting is fun", due_date: DateTime.now, image: File.new("#{Rails.root}/public/empty.jpg"))
assignment3 = Assignment.create(name: "Multiplication", description: "Multiplying is fun", due_date: DateTime.now, image: File.new("#{Rails.root}/public/empty.jpg"))
assignment4 = Assignment.create(name: "Division", description: "Division is fun", due_date: DateTime.now, image: File.new("#{Rails.root}/public/empty.jpg"))

course.assignments << assignment1 << assignment2 << assignment3 << assignment4

User.create(username: "Student", first_name: "Student", last_name: "Student", email: "student@student.com", password: "student", password_confirmation: "student", role: "Student")

admin = User.create(username: "admin", first_name: "admin", last_name: "admin", email: "admin@admin.com", password: "adminadmin", password_confirmation: "adminadmin", role: "Teacher")


mark = User.create(username: "Mark", first_name: "Mark", last_name: "Mark", email: "mark@mark.com", password: "pizzahut", password_confirmation: "pizzahut", role: "Student")
aaron = User.create(username: "Aaron", first_name: "Aaron", last_name: "Aaron", email: "aaron@aaron.com", password: "piehut", password_confirmation: "piehut", role: "Student")
daniel = User.create(username: "Daniel", first_name: "Daniel", last_name: "Daniel", email: "daniel@daniel.com", password: "doughnut", password_confirmation: "doughnut", role: "Student")

mark.submissions.new(assignment: assignment1, status: "Incomplete", image: File.new("#{Rails.root}/public/empty.jpg")).save
mark.submissions.new(assignment: assignment2, status: "Incomplete", image: File.new("#{Rails.root}/public/empty.jpg")).save
mark.submissions.new(assignment: assignment3, status: "Complete", image: File.new("#{Rails.root}/public/empty.jpg")).save
mark.submissions.new(assignment: assignment4, status: "Complete", image: File.new("#{Rails.root}/public/empty.jpg")).save

aaron.submissions.new(assignment: assignment1, status: "Complete", image: File.new("#{Rails.root}/public/empty.jpg")).save
aaron.submissions.new(assignment: assignment2, status: "Incomplete", image: File.new("#{Rails.root}/public/empty.jpg")).save
aaron.submissions.new(assignment: assignment3, status: "Incomplete", image: File.new("#{Rails.root}/public/empty.jpg")).save
aaron.submissions.new(assignment: assignment4, status: "Complete", image: File.new("#{Rails.root}/public/empty.jpg")).save

daniel.submissions.new(assignment: assignment1, status: "Incomplete", image: File.new("#{Rails.root}/public/empty.jpg")).save
daniel.submissions.new(assignment: assignment2, status: "Incomplete", image: File.new("#{Rails.root}/public/empty.jpg")).save
daniel.submissions.new(assignment: assignment3, status: "Complete", image: File.new("#{Rails.root}/public/empty.jpg")).save
daniel.submissions.new(assignment: assignment4, status: "Complete", image: File.new("#{Rails.root}/public/empty.jpg")).save

admin.course_user_joins.create(course: course)
mark.course_user_joins.create(course: course)
aaron.course_user_joins.create(course: course)
daniel.course_user_joins.create(course: course)
