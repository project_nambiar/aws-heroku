require 'test_helper'

class ApisControllerTest < ActionController::TestCase
  test "User will be redirected to khan on a button press" do
    get :new
    assert 200 #success (200 is OK for webpages)
  end

  test "User will be redirected to khan for an authenticated call on a button press" do
    get :create
    assert 200 #success
  end
end
