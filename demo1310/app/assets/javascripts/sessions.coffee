# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

email_validation = (value) ->
    regex = /^([\w.-]+)@([\w.-]+)\.([a-zA-Z.]{2,6})$/i

    if value is ""
        $("#login-email-error").text("can't be blank")
        return false
    else if !(value.match regex)
        $("#login-email-error").text("is invalid email")
        return false
    else
        $("#login-email-error").text("")
        return true

password_validation = (value) ->
    if value is ""
        $("#login-password-error").text("is too short (at least 6 characters)")
        return false
    else if value.length < 6
        $("#login-password-error").text("is too short (at least 6 characters)")
        return false
    else if value.length > 20
        $("#login-password-error").text("is too long (maximum is 20 characters)")
        return false
    else
        $("#login-password-error").text("")
        return true

buttonEnable = () ->
    result = password_validation($("#password").val()) && email_validation($("#email").val())
    if result is true
        $('#submit-button').prop('disabled', false)
    else
        $('#submit-button').prop('disabled', true)

jQuery ->
    $("#email").focusout ->
        email_validation(@value)
        buttonEnable()

    $("#password").focusout ->
        password_validation(@value)
        buttonEnable()
