class CourseStudentsController < ApplicationController

  before_action :require_teacher, only: [:show_students, :add_students, :remove_students, :create_course, :new_course]

  def show_students
    @course = Course.find(params[:course_id])
    @enrolled_students = @course.users.all
    @unenrolled_students = User.all - @course.users.all
  end

  def add_students
    course = Course.find(params[:course_id])
    student = User.find(params[:student_id])
    course.course_user_joins.create(:user => student)
      course.assignments.each do |a|
        isAssign = false
        student.submissions.each do |s|
          if s.assignment == a
             isAssign = true
          end
        end
        if !isAssign
          student.submissions.create(assignment: a, status: "Incomplete", image: File.new("#{Rails.root}/public/empty.jpg"))
        end
      end
    redirect_to :back
  end

  def remove_students
    course = Course.find(params[:course_id])
    student = User.find(params[:student_id])
    course.course_user_joins.where(:user => student).destroy_all
    redirect_to :back
  end

  def create_course
    user = User.find(session[:user_id])
    @course = Course.create(course_params)
    if @course.valid?
      @course.course_user_joins.create(:user => user)
      redirect_to root_path
    else
      flash[:danger] = "Course code and/or name is already in use."
      redirect_to new_course_path
    end
end

  def new_course
    @course = Course.new
  end

  private

    def course_params
      params.require(:course).permit(:code, :description, :name)
    end

end
