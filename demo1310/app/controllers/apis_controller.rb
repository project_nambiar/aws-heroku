class ApisController < ApplicationController

	$assignment_id = nil

	def new
		$assignment_id = params[:assignment_id]
		redirect_to "/auth/khan_academy"
	end

  def create
    begin
  		@omni = request.env['omniauth.auth'] #Calls user.rb from_omniauth(auth_hash)

     	consumer = OAuth::Consumer.new(
        CONSUMER_TOKEN,
        CONSUMER_SECRET,
        :site => "https://www.khanacademy.org",
        :scheme => :header
      )

      access_token = OAuth::AccessToken.from_hash(
        consumer,
        :oauth_token => @omni['credentials']['token'],
        :oauth_token_secret => @omni['credentials']['secret']
      )
    	
      response = access_token.get("http://www.khanacademy.org/api/v1/user/exercises")
      @hash = response.body
      @user_api = JSON.parse(@hash)

      assignment = Assignment.find($assignment_id)

      found = false
      @user_api.each do |e|
      	found = true if e["exercise_model"]["display_name"] == assignment.name
      end

      if found
      	flash[:success] = "Congratulations! You are now ready to submit."
      else
      	flash[:failure] = "Looks like you haven't completed the exercise."
      end

      redirect_to edit_submission_path(assignment_id: $assignment_id, submission_state: found)

    rescue
      render text: 'Error: Not Authenticated with Khan'
    end
  end

end
