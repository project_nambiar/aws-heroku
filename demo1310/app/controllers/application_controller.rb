class ApplicationController < ActionController::Base

  protect_from_forgery with: :exception

  helper_method :current_user
  helper_method :require_user
  helper_method :require_teacher
  helper_method :require_student

  def current_user
    @current_user ||= User.find(session[:user_id]) if session[:user_id]
  end

  def require_user
    redirect_to '/login' unless current_user
  end

  def require_teacher
    redirect_to '/' unless current_user.teacher?
  end

  def require_student
    redirect_to '/' unless current_user.student?
  end
end
