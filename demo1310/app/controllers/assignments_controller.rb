class AssignmentsController < ApplicationController

  before_action :require_teacher, only: [:new, :create, :edit, :update, :destroy]

  def index
    if params[:course_id]
      @course = Course.find(params[:course_id])
      @assignments = @course.assignments
    else
      redirect_to root_path
    end
  end

  def new
    @assignment = Assignment.new
    @course = Course.find(params[:course_id])
    @type = params[:assign_type]
    #khan courses private variable
    if @type == "khan"
      @khan = JSON.parse(File.read("#{Rails.root}/public/khan-exercises.json"))
      @khan_exercises = Array.new
      @khan.each do |k|
        @khan_exercises += [k["display_name"]]
      end
      @khan_exercises.sort!
    end
  end

  def edit
    @assignment = Assignment.find(params[:id])

    #khan courses private variable
    if @assignment.khan_exercise
      @khan = JSON.parse(File.read("#{Rails.root}/public/khan-exercises.json"))
      @khan_exercises = Array.new
      @khan.each do |k|
        @khan_exercises += [k["display_name"]]
      end
    end
  end



  def show
    @assignment = Assignment.find(params[:id])
    @course = @assignment.course
  end

  def create
    type = params[:assign_type]
    @course = Course.find(params[:course_id])
    students = @course.users.where("role = 'Student'")

    @assignment = Assignment.new(assignment_params)

    khan_exercise = nil
    if type == "khan"
      khan = JSON.parse(File.read("#{Rails.root}/public/khan-exercises.json"))
      khan.each do |k|
        khan_exercise = k if k["display_name"] == @assignment.khan_exercise
      end
      @assignment.name = khan_exercise["pretty_display_name"]
      @assignment.khan_exercise = khan_exercise["ka_url"]
      @assignment.description = khan_exercise["description"]
    end

    if @assignment.save
      students.each do |s|
        s.submissions.create(assignment: @assignment, status: "Incomplete", image: File.new("#{Rails.root}/public/empty.jpg"))
      end
      @course.assignments << @assignment
      redirect_to @assignment
    else
      render 'new', course_id: @course.id, assign_type: type
    end
  end

  def update

    @assignment = Assignment.find(params[:id])

    @assignment.submissions.each do |submission|
      submission.status = "Incomplete"
      submission.save
    end

    if @assignment.khan_exercise
      khan_exercise = ""
      khan = JSON.parse(File.read("#{Rails.root}/public/khan-exercises.json"))
      khan.each do |k|
        khan_exercise = k if k["display_name"] == assignment_params[:khan_exercise]
      end
      @assignment.name = khan_exercise["pretty_display_name"]
      @assignment.khan_exercise = khan_exercise["ka_url"]
      @assignment.description = khan_exercise["description"]
      @assignment.due_date = assignment_params[:due_date]
      @assignment.save

    end

    @assignment.update(assignment_params)

    redirect_to @assignment

  end

  def destroy
    @assignment = Assignment.find(params[:id])
    @course = @assignment.course
    @assignment.destroy

    redirect_to @course
  end



  private
  def assignment_params
    params.require(:assignment).permit(:name, :khan_exercise, :description, :due_date, :image)
  end



end
