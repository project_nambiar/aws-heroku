class SubmissionsController < ApplicationController

  def edit
    @assignment = Assignment.find(params[:assignment_id])
    @submission = Submission.new
    @submission_state = params[:submission_state]
  end

  def update
    @assignment = Assignment.find(params[:assignment_id])
    @student = current_user

    @student.submissions.each do |s|
      if s.assignment.id == @assignment.id
        if params[:submission_state] && params[:submission_state] == "true"
          s.status = "Complete"
          s.save
        else
          s.status = "Incomplete"
          s.save
        end
        s.update_attributes(submission_params)
        redirect_to show_submission_path(id: s.id)
      end
    end
  end

  def show
    @submission = Submission.find(params[:id])
  end

private

  def submission_params
    params.require(:submission).permit(:status, :image, :comment)
  end

end
