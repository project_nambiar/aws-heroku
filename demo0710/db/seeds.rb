# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or newd alongside the db with db:setup).
#
# Examples:
#
#   cities = City.new([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.new(name: 'Emanuel', city: cities.first)
#

course = Course.new(code: "Math101", name: "Statistics", description: "Learn some math")

assignment1 = Assignment.create(name: "Addition", description: "Adding is fun", due_date: DateTime.now)
assignment2 = Assignment.create(name: "Subtraction", description: "Subtracting is fun", due_date: DateTime.now)
assignment3 = Assignment.create(name: "Multiplication", description: "Multiplying is fun", due_date: DateTime.now)
assignment4 = Assignment.create(name: "Division", description: "Division is fun", due_date: DateTime.now)

course.assignments << assignment1 << assignment2 << assignment3 << assignment4
course.save


mark = User.create(username: "Mark", first_name: "Mark", last_name: "Mark", email: "mark@mark.com", password: "pizzahut", password_confirmation: "pizzahut", role: "Student")
aaron = User.create(username: "Aaron", first_name: "Aaron", last_name: "Aaron", email: "aaron@aaron.com", password: "piehut", password_confirmation: "piehut", role: "Student")
daniel = User.create(username: "Daniel", first_name: "Daniel", last_name: "Daniel", email: "daniel@daniel.com", password: "doughnut", password_confirmation: "doughnut", role: "Student")

mark.submissions.create!(assignment: assignment1, status: "Incomplete")
mark.submissions.create!(assignment: assignment2, status: "Pending")
mark.submissions.create!(assignment: assignment3, status: "Complete")
mark.submissions.create!(assignment: assignment4, status: "Pending")

aaron.submissions.create!(assignment: assignment1, status: "Pending")
aaron.submissions.create!(assignment: assignment2, status: "Pending")
aaron.submissions.create!(assignment: assignment3, status: "Incomplete")
aaron.submissions.create!(assignment: assignment4, status: "Complete")

daniel.submissions.create!(assignment: assignment1, status: "Incomplete")
daniel.submissions.create!(assignment: assignment2, status: "Pending")
daniel.submissions.create!(assignment: assignment3, status: "Complete")
daniel.submissions.create!(assignment: assignment4, status: "Incomplete")

User.create(username: "admin", first_name: "admin", last_name: "admin", email: "admin@admin.com", password: "adminadmin", password_confirmation: "adminadmin", role: "Teacher")
