require 'test_helper'

class UsersControllerTest < ActionController::TestCase

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should get new with user" do
    get :new
    assert_response :success
    assert_not_nil assigns(:user)
  end

  test "should create a user" do
    post :create, user: {username: "judykay", first_name: "Judy", last_name: "Kay", email: "judy@mail.com", password: "judykay1", password_confirmation: "judykay1", role: "teacher"}
    assert_not_nil assigns(:user)
  end

  test "should create a teacher user" do
    post :create, user: {username: "judykay", first_name: "Judy", last_name: "Kay", email: "judy@mail.com", password: "judykay1", password_confirmation: "judykay1", role: "teacher"}
    user = User.find(session[:user_id])
    assert(user.teacher?)
  end

  test "should create a student user" do
    post :create, user: {username: "judykay", first_name: "Judy", last_name: "Kay", email: "judy@mail.com", password: "judykay1", password_confirmation: "judykay1", role: "student"}
    user = User.find(session[:user_id])
    assert(user.student?)
  end

  test "should create user and redirect" do
    assert_difference('User.count') do
      post :create, user: {username: "judykay", first_name: "Judy", last_name: "Kay", email: "judy@mail.com", password: "judykay1", password_confirmation: "judykay1", role: "student"}
    end
    assert_redirected_to "/"
  end

  test "should redirect if create data is invalid" do
    assert_no_difference('User.count') do
      post :create, user: {username: "judykay", password: "judykay1"}
    end

    assert_redirected_to "/signup"
  end

  test "should update user" do
    post :create, user: {username: "judykay", first_name: "Judy", last_name: "Kay", email: "judy@mail.com", password: "judykay1", password_confirmation: "judykay1", role: "teacher"}
    user = User.find(session[:user_id])
    assert_equal("Judy", user.first_name)
    patch :update, user: {first_name: "Judas", last_name: "Kay", email: "judy@mail.com"}
    user = User.find(session[:user_id])
    assert_equal("Judas", user.first_name)
  end

  test "should reject user update with wrong data" do
    post :create, user: {username: "judykay", first_name: "Judy", last_name: "Kay", email: "judy@mail.com", password: "judykay1", password_confirmation: "judykay1", role: "teacher"}
    patch :update, user: {first_name: nil, last_name: "Kay", email: "judy@mail.com"}
    assert_redirected_to "/edit"
  end

  test "should delete an account" do
    delete :destroy
    assert_redirected_to "/login"
  end

end
