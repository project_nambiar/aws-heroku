require 'test_helper'

class CourseStudentsControllerTest < ActionController::TestCase
  test "should get show_students" do
    get :show_students
    assert_response :success
  end

  test "should get add_students" do
    get :add_students
    assert_response :success
  end

  test "should get remove_students" do
    get :remove_students
    assert_response :success
  end

end
