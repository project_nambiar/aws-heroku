require 'test_helper'

class CourseUserJoinTest < ActiveSupport::TestCase

  test "ensure that a User can have a Course" do
		user = User.create(username: "judykay", first_name: "Judy", last_name: "Kay", email: "judy@mail.com", password: "judykay1", password_confirmation: "judykay1")
		phys_course = Course.create(code: "PHYS0101", name: "Physics 101", description: "An introductory course for Physics")
		user.course_user_join.create(course: phys_course)
		assert_instance_of(Course, user.course.first, msg="relationship not there")
	end

	test "ensure that a Course can have a User" do
		user = User.create(username: "judykay", first_name: "Judy", last_name: "Kay", email: "judy@mail.com", password: "judykay1", password_confirmation: "judykay1")
		phys_course = Course.create(code: "PHYS0101", name: "Physics 101", description: "An introductory course for Physics")
		phys_course.course_user_join.create(user: user)
		assert_instance_of(User, phys_course.user.first, msg="relationship not there")
	end

end
