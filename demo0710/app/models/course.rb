class Course < ActiveRecord::Base
  has_many :course_user_joins
  has_many :users, through: :course_user_joins

  has_many :assignments
  #Validations
  validates(:code, :name,
            presence: true,
            length: {minimum: 4, maximum: 15},
            uniqueness: {case_sensitive:true})

  validates :description,
    length: {maximum: 300}

end
