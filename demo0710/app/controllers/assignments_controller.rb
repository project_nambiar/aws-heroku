class AssignmentsController < ApplicationController
  def new
    @assignment = Assignment.new
    @course = Course.find(session[:course_id])

    #khan courses private variable
    @khan = File.read("#{Rails.root}/public/khan-exercises.json")
    @kcourses = JSON.parse(@khan)
  end

  def edit
    @assignment = Assignment.find(params[:id])
  end

  def index
    @assignment = Assignment.all
  end

  def show
    @assignment = Assignment.find(params[:id])
    @course = @assignment.course
  end

  def create

    @course = Course.find(session[:course_id])
    @assignment = Assignment.new(assignment_params)


    if @assignment.save
      @course.assignments << @assignment
      redirect_to @assignment
    else
      render 'new'
    end
  end

  def update

    @assignment = Assignment.find(params[:id])

    if @assignment.update(assignment_params)
      redirect_to @assignment
    else
      render 'edit'
    end
  end

  def destroy

    @assignment = Assignment.find(params[:id])
    @assignment.destroy

    redirect_to assignments_path
  end



  private
  def assignment_params
    params.require(:assignment).permit(:name,:description,:due_date)
  end



end
