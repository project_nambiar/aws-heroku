require 'test_helper'

class TaskTest < ActiveSupport::TestCase

  test "ensure that a new Task object is an instance of a Task Model" do
  	course = Course.create(code: "PHYS0101", name: "Physics 101", description: "An introductor tasks for Physics")
		task = Task.create(name: "Assignment1", description: "Write a proposal document.", due_date: DateTime.new(2015,9,4), course: course)
  	assert_instance_of(Task, task, msg="not a Task object")
  end

	test "ensure that a new Task object with data is not nil" do
  	course = Course.create(code: "PHYS0101", name: "Physics 101", description: "An introductor tasks for Physics")
		task = Task.create(name: "Assignment1", description: "Write a proposal document.", due_date: DateTime.new(2015,9,4), course: course)
		assert_not_nil(task, msg="nil")
	end

	test "ensure that a new Task object with data is not empty" do
  	course = Course.create(code: "PHYS0101", name: "Physics 101", description: "An introductor tasks for Physics")
		task = Task.create(name: "Assignment1", description: "Write a proposal document.", due_date: DateTime.new(2015,9,4), course: course)
		assert_not_nil(task.name, msg="empty")
		assert_not_nil(task.description, msg="empty")
		assert_not_nil(task.due_date, msg="empty")
		assert_not_nil(task.course, msg="empty")
	end

	test "ensure that a new Task name is stored correctly" do
   	course = Course.create(code: "PHYS0101", name: "Physics 101", description: "An introductor tasks for Physics")
		task = Task.create(name: "Assignment1", description: "Write a proposal document.", due_date: DateTime.new(2015,9,4), course: course)
		assert_equal("Assignment1", task.name, msg="not a valid task name")
	end

	test "ensure that a new Task description is stored correctly" do
  	course = Course.create(code: "PHYS0101", name: "Physics 101", description: "An introductor tasks for Physics")
		task = Task.create(name: "Assignment1", description: "Write a proposal document.", due_date: DateTime.new(2015,9,4), course: course)
		assert_equal("Write a proposal document.", task.description, msg="not a valid description")
	end

	test "ensure that a new Task due date is stored correctly" do
  	course = Course.create(code: "PHYS0101", name: "Physics 101", description: "An introductor tasks for Physics")
  	due_date = DateTime.new(2015,9,4)
		task = Task.create(name: "Assignment1", description: "Write a proposal document.", due_date: due_date, course: course)
		assert_equal(due_date, task.due_date, msg="not a valid due date")
	end

	test "ensure that a new Task Course reference is stored correctly" do
  	course = Course.create(code: "PHYS0101", name: "Physics 101", description: "An introductor tasks for Physics")
		task = Task.create(name: "Assignment1", description: "Write a proposal document.", due_date: DateTime.new(2015,9,4), course: course)
		assert_equal(course, task.course, msg="not a valid reference")
	end

end
