class CoursesController < ApplicationController
  before_action :set_course, only: [:show, :edit, :update, :destroy]
  before_action :require_user, only: [:index, :show, :new, :edit, :destroy, :update, :create]

  def index
    @courses = Course.all
  end

  def show
  end

  def new
    @course = Course.new
  end

  def edit
  end
 
  def create
    @course = Course.new(course_params)
    flash[:notice] = "Task was successfully created." if @course.save
    redirect_to @course
  end

  def update
    flash[:notice] = "Task was successfully updated." if @course.update(course_params)
    redirect_to @course
  end

  def destroy
    @course.destroy
    flash[:notice] = "Course was successfully destroied."
    redirect_to courses_url
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_course
      @course = Course.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def course_params
      params.require(:course).permit(:code, :description, :name)
    end
end
