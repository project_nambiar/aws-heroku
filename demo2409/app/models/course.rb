class Course < ActiveRecord::Base
	has_many :course_user_join
	has_many :user, through: :course_user_join

	#Validations
	validates(:code, :name, 
		presence: true,
		length: {minimum: 4, maximum: 15},
		uniqueness: {case_sensitive:true})
	
	validates :description,
		length: {maximum: 300}

end
