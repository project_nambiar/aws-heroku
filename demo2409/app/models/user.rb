class User < ActiveRecord::Base

	has_many :course_user_join
	has_many :course, through: :course_user_join

	has_secure_password

  def teacher? 
    self.role == 'teacher' 
  end

  def student? 
    self.role == 'student' 
  end
	
  EMAIL_REGEX = /\A([\w+\-].?)+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/i
  validates :username, :presence => true, :uniqueness => true, :length => { :in => 3..20 }
  validates :email, :presence => true, :uniqueness => true, :format => EMAIL_REGEX
  validates :password, :confirmation => true
  validates_length_of :password, :in => 6..20, :on => :create
  validates :first_name, :presence => true
  validates :last_name, :presence => true
end
