Rails.application.routes.draw do
  resources :courses
  resources :users

  root "courses#index"

  get "signup" => "users#new"
  post "signup" => "users#create"

  get 'login' => 'sessions#new'
  post 'login' => 'sessions#create'

  delete 'logout' => 'sessions#destroy'

  delete 'courses/:id' => 'courses#destroy'

  get 'edit' => 'users#edit'
  patch 'edit' => 'users#update'
  delete 'delete' => 'users#destroy'
end
