class CreateSubmissions < ActiveRecord::Migration
  def change
    create_table :submissions do |t|
      t.belongs_to :user
      t.belongs_to :assignment
      t.string :status
      t.timestamps null: false
    end
  end
end
