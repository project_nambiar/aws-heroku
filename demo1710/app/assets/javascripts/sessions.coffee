# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

formValidation = {}
formValidation.email = true
formValidation.password = true

email_validation = (value) ->
    regex = /^([\w.-]+)@([\w.-]+)\.([a-zA-Z.]{2,6})$/i

    if value is ""
        $("#login-email-error").text("can't be blank")
        return false
    else if !(value.match regex)
        $("#login-email-error").text("is invalid email")
        return false
    else
        $("#login-email-error").text("")
        return true

password_validation = (value) ->
    if value is ""
        $("#login-password-error").text("is too short (at least 6 characters)")
        return false
    else if value.length < 6
        $("#login-password-error").text("is too short (at least 6 characters)")
        return false
    else if value.length > 20
        $("#login-password-error").text("is too long (maximum is 20 characters)")
        return false
    else
        $("#login-password-error").text("")
        return true

buttonEnable = () ->
    result = formValidation.email && formValidation.password
    if result is true
        $('#submit-button').prop('disabled', false)
    else
        $('#submit-button').prop('disabled', true)

jQuery ->
    $("#email").keyup ->
        formValidation.email = email_validation(@value)
        buttonEnable()

    $("#password").keyup ->
        formValidation.password = password_validation(@value)
        buttonEnable()
