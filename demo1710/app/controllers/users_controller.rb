class UsersController < ApplicationController

  before_action :require_user, only: [:edit, :update, :destroy, :show_courses]

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)

    if @user.save
      session[:user_id] = @user.id
      flash[:success] = "Congratulations! You have created an account."
      redirect_to "/"
    else
      flash[:danger] = "Username and/or email already exists."
      redirect_to "/signup"
    end
  end

  def edit
    @user = User.find(session[:user_id])
  end

  def update
    @user = User.find(session[:user_id])

    if @user.update_attributes(update_user_params)
      flash[:success] = "User Updated"
      redirect_to "/"
    else
      flash[:danger] = "Invalid Data"
      redirect_to "/edit"
    end
  end

  def destroy
    @user = User.find(session[:user_id])
    session[:user_id] = nil
    @user.destroy
    redirect_to "/login"
  end

  def show_courses
    user = User.find(session[:user_id])
    @courses = user.courses
  end

  private

    def user_params
      params.require(:user).permit(:role, :username, :first_name, :last_name, :email, :password, :password_confirmation)
    end

    def update_user_params
      params.require(:user).permit(:first_name, :last_name, :email)
    end

end
