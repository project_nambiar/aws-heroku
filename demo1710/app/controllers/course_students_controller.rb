class CourseStudentsController < ApplicationController

  before_action :require_user, only: [:show_students, :add_students, :remove_students, :create_course, :new_course]
  before_action :require_teacher, only: [:show_students, :add_students, :remove_students, :create_course, :new_course]

  def show_students
    @course = Course.find(params[:course_id])
    @enrolled_users = @course.users.all
    @teachers = @enrolled_users.where("role == 'Teacher'").count
    @enrolled_users = @enrolled_users.sort_by{|eu| [eu.role, eu.last_name]}
    @unenrolled_users = User.all - @course.users.all
    @unenrolled_users = @unenrolled_users.sort_by{|uu| [uu.role, uu.last_name]} if @unenrolled_users.nil?
  end

  def add_students
    course = Course.find(params[:course_id])
    student = User.find(params[:student_id])
    course.course_user_joins.create(:user => student)

    course.assignments.each do |a|
      if student.role == "Student" && student.submissions.where("assignment_id == #{a.id}").empty?
        student.submissions.create(assignment: a, status: "Not Yet Started", image: nil)
      end
    end
    redirect_to :back
  end

  def remove_students
    course = Course.find(params[:course_id])
    user = User.find(params[:student_id])
    course.users.delete(user)
    redirect_to :back
  end

  def create_course
    user = User.find(session[:user_id])
    @course = Course.create(course_params)
    if @course.valid?
      @course.course_user_joins.create(:user => user)
      redirect_to root_path
    else
      flash[:danger] = "Course code and/or name is already in use."
      redirect_to new_course_path
    end
  end

  def new_course
    @course = Course.new
  end

  private

  def course_params
    params.require(:course).permit(:code, :description, :name)
  end

end

