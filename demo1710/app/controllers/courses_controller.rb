class CoursesController < ApplicationController
  before_action :set_course, only: [:show, :edit, :update, :destroy]
  before_action :require_user, only: [:index, :show, :new, :edit, :destroy, :update, :create]
  before_action :require_teacher, only: [:index, :new, :edit, :destroy, :update, :create]

  def index
    @courses = Course.all
  end

  def show
    @assignments = @course.assignments
    @assignments = @assignments.sort_by{|a| a.name}
    @enrolled_students = @course.users.where("role='Student'")
    @enrolled_students = @enrolled_students.sort_by{|es| es.last_name}
  end

  def new
    @course = Course.new
  end

  def edit
  end

  def create
    @course = Course.new(course_params)

    if @course.save
      @course.course_user_joins.create(:user => current_user)
      flash[:success] = "Course was successfully created."
    end

    redirect_to @course
  end

  def update
    if @course.update(course_params)
      flash[:success] = "Course was successfully updated."
      redirect_to @course
    else
      flash[:danger] = "Course code and/or name currently exists."
      redirect_to edit_course_path
    end
  end

  def destroy
    @course.destroy
    flash[:success] = "Course was successfully destroyed."
    redirect_to root_path
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_course
      @course = Course.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def course_params
      params.require(:course).permit(:code, :description, :name)
    end
end
