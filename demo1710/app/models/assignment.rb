class Assignment < ActiveRecord::Base
  belongs_to :course

  has_many :submissions
  has_many :user, through: :submissions

  has_attached_file :image
  validates_attachment_content_type :image, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]

  validates_presence_of :name
end
