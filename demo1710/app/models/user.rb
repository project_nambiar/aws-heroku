class User < ActiveRecord::Base

  has_many :course_user_joins
  has_many :courses, through: :course_user_joins

  has_many :submissions
  has_many :assignment, through: :submissions

  has_secure_password

  def teacher?
    self.role == 'Teacher'
  end

  def student?
    self.role == 'Student'
  end

  EMAIL_REGEX = /\A([\w+\-].?)+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/i
  validates :username, :presence => true, :uniqueness => true, :length => { :in => 3..20 }
  validates :email, :presence => true, :uniqueness => true, :format => EMAIL_REGEX
  validates :password, :confirmation => true
  validates_length_of :password, :in => 6..20, :on => :create
  validates :first_name, :presence => true
  validates :last_name, :presence => true
end
