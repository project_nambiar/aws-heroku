require 'test_helper'

class UserTest < ActiveSupport::TestCase

  test "ensure that a new User object is an instance of a User Model" do
    user = User.create(username: "judykay", first_name: "Judy", last_name: "Kay", email: "judy@mail.com", password: "judykay1", password_confirmation: "judykay1")
    assert_instance_of(User, user, msg="not a User object")
  end

  test "ensure that a new User object with data is not nil" do
    user = User.create(username: "judykay", first_name: "Judy", last_name: "Kay", email: "judy@mail.com", password: "judykay1", password_confirmation: "judykay1")
    assert_not_nil(user, msg="nil")
  end

  test "ensure that a new User object with data is not empty" do
    user = User.create(username: "judykay", first_name: "Judy", last_name: "Kay", email: "judy@mail.com", password: "judykay1", password_confirmation: "judykay1")
    assert_not_nil(user.username, msg="empty")
    assert_not_nil(user.first_name, msg="empty")
    assert_not_nil(user.last_name, msg="empty")
    assert_not_nil(user.email, msg="empty")
    assert_not_nil(user.password_digest, msg="empty")
  end

  test "ensure that a new User username is stored correctly" do
    user = User.create(username: "judykay", first_name: "Judy", last_name: "Kay", email: "judy@mail.com", password: "judykay1", password_confirmation: "judykay1")
    assert_equal("judykay", user.username, msg="not the same username as passed")
  end

  test "ensure that a new User first name is stored correctly" do
    user = User.create(username: "judykay", first_name: "Judy", last_name: "Kay", email: "judy@mail.com", password: "judykay1", password_confirmation: "judykay1")
    assert_equal("Judy", user.first_name, msg="not the same first name as passed")
  end

  test "ensure that a new User last name is stored correctly" do
    user = User.create(username: "judykay", first_name: "Judy", last_name: "Kay", email: "judy@mail.com", password: "judykay1", password_confirmation: "judykay1")
    assert_equal("Kay", user.last_name, msg="not the same last name as passed")
  end

  test "ensure that a new User email is stored correctly" do
    user = User.create(username: "judykay", first_name: "Judy", last_name: "Kay", email: "judy@mail.com", password: "judykay1", password_confirmation: "judykay1")
    assert_equal("judy@mail.com", user.email, msg="not the same email as passed")
  end

  test "ensure that a new User password is hashed" do
    user = User.create(username: "judykay", first_name: "Judy", last_name: "Kay", email: "judy@mail.com", password: "judykay1", password_confirmation: "judykay1")
    assert_not_equal("judykay1", user.password_digest, msg="password not hashed")
  end

end
