require 'test_helper'

class KhanExerciseTest < ActiveSupport::TestCase
  test "ensure that a new Khan Exercise object is an instance of a User Model" do
    exer = KhanExercise.create(name: "Test")
    assert_instance_of(KhanExercise, exer, msg="not a Khan Exercise object")
  end

  test "ensure that a new Khan Exercise object with data is not nil" do
    exer = KhanExercise.create(name: "Test")
    assert_not_nil(exer, msg="nil")
  end

  test "ensure that a new User object with data is not empty" do
    exer = KhanExercise.create(name: "test")
    assert_not_nil(exer.name, msg="empty")
  end

  test "ensure that a new Khan Exercise username is stored correctly" do
    exer = KhanExercise.create(name: "test")
    assert_equal("test", exer.name, msg="not the same name as passed")
  end
end
