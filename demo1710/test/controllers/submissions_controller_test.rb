require 'test_helper'

class SubmissionsControllerTest < ActionController::TestCase
  #update for status of the submission. I.E. Complete -> Incomplete
  test "should update a submission" do
    submission = Submission.create(status: "Complete", image: File.new("#{Rails.root}/public/empty.jpg"))
    submission.update(status:'Incomplete')
    assert_equal('Incomplete', submission.status)
  end
end
