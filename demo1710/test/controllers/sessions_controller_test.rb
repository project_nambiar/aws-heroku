require 'test_helper'

class SessionsControllerTest < ActionController::TestCase

  test "should create a valid session upon signup" do
    user = User.create(username: "judykay", first_name: "Judy", last_name: "Kay", email: "judy@mail.com", password: "judykay1", password_confirmation: "judykay1")
    post :create, session: {email: "judy@mail.com", password: "judykay1"}
    assert_not_nil(session[:user_id])
    assert_equal(user.id, session[:user_id])
  end

  test "should create a session upon signup and redirect" do
    User.create(username: "judykay", first_name: "Judy", last_name: "Kay", email: "judy@mail.com", password: "judykay1", password_confirmation: "judykay1")
    post :create, session: {email: "judy@mail.com", password: "judykay1"}
    assert_redirected_to('/')
  end

  test "should redirect to login upon invalid login" do
    User.create(username: "x", first_name: "Judy", last_name: "Kay", email: "judy@mail.com", password: "judykay1")
    post :create, session: {email: "judy@mail.com", password: "judykay1"}
    assert_redirected_to('/login')
  end

  test "should destroy a session" do
    delete :destroy
    assert_nil(session[:user_id])
  end

  test "should redirect after destroying a session" do
    delete :destroy
    assert_redirected_to("/login")
  end

end
