require 'test_helper'

class AssignmentsControllerTest < ActionController::TestCase

  #new
  test "should go to new assignment page" do
    #Create a course first so the assignment will be created for this course
    user = User.create(username: "judykay", first_name: "Judy", last_name: "Kay", email: "judy@mail.com", password: "judykay1", password_confirmation: "judykay1", role: "Teacher")
    session[:user_id] = user.id
    test_course = Course.create(code: "TEST101",
                                name: "Test",
                                description:"bla")
    get :new, course_id: test_course.id
    assert_response :success
  end

  #create
  test "should create assignment" do
    #check after doing inner code that assignment count has changed
    assert_difference('Assignment.count') do
      #Create a course first so the assignment will be created for this course
      Course.create(code: "TEST101",
                    name: "Test",
                    description:"bla")
      #Create the assignment
      Assignment.create(name: 'Test assignment name', description: 'blablabla', due_date: DateTime.new(2027), image: File.new("#{Rails.root}/public/empty.jpg"))
    end
  end

  #create and redirect
  test "should create an assignment and redirect" do
    user = User.create(username: "judykay", first_name: "Judy", last_name: "Kay", email: "judy@mail.com", password: "judykay1", password_confirmation: "judykay1", role: "Teacher")
    session[:user_id] = user.id
    assert_difference('Assignment.count') do
      #Create a course first so the assignment will be created for this course
      course_test = Course.create(code: "TEST101", name: "Test", description:"bla")
      img = Rack::Test::UploadedFile.new("#{Rails.root}/public/empty.jpg", 'image/jpg')
      post :create, assignment: {name: 'Test assignment name', description: 'blablabla', due_date: DateTime.new(2027), image: img}, course_id: course_test.id
    end
    assert_redirected_to assignment_path(assigns(:assignment))
  end

  #update
  test "should update an assignment" do
    assignment = Assignment.create(name: 'testAssignment',
                                   description: 'blabla',
                                   due_date: DateTime.new(2027))
    assignment.update(name:'AssignmentChanged')
    assert_equal('AssignmentChanged', assignment.name)
  end

  #destroy
  test "should destroy an assignment" do
    assert_no_difference('Assignment.count') do
      #Create a course first so the assignment will be created for this course
      Course.create(code: "TEST101",
                    name: "Test",
                    description:"bla")
      assignment = Assignment.create(name: 'Test assignment name',
                                     description: 'blablabla',
                                     due_date: DateTime.new(2027))
      assignment.delete()
    end
  end

  test "should route to assignment index when an assignment is destroyed" do
    assert_routing({method: 'delete', path:'/assignment'}, {controller: "assignments", action: "destroy"})
  end

end


