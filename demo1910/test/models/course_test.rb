require 'test_helper'

class CourseTest < ActiveSupport::TestCase

  test "ensure that a new Course object is an instance of a Course Model" do
    phys_course = Course.create(code: "PHYS0101", name: "Physics 101", description: "An introductory course for Physics")
    assert_instance_of(Course, phys_course, msg="not a Course object")
  end

  test "ensure that a new Course object with data is not nil" do
    course = Course.create(code: "PHYS0101", name: "Physics 101", description: "An introductory course for Physics")
    assert_not_nil(course, msg="nil")
  end

  test "ensure that a new Course object with data is not empty" do
    course = Course.create(code: "PHYS0101", name: "Physics 101", description: "An introductory course for Physics")
    assert_not_nil(course.id, msg="empty")
    assert_not_nil(course.code, msg="empty")
    assert_not_nil(course.name, msg="empty")
    assert_not_nil(course.description, msg="empty")
  end

  test "ensure that a new Course has a valid course code" do
    course = Course.create(code: "PHYS0101", name: "Physics 101", description: "An introductory course for Physics")
    assert_equal("PHYS0101", course.code, msg="not a valid course code")
  end

  test "ensure that a new Course has a valid name" do
    course = Course.create(code: "PHYS0101", name: "Physics 101", description: "An introductory course for Physics")
    assert_equal("Physics 101", course.name, msg="not a valid name")
  end

  test "ensure that a new Course has a valid description" do
    course = Course.create(code: "PHYS0101", name: "Physics 101", description: "An introductory course for Physics")
    assert_equal("An introductory course for Physics", course.description, msg="not a valid description")
  end

  #####################################
  #Validations testing
  #####################################
  def setup
    @course = Course.new(code: "MATH1005", name: "Statistics", description: "This is a good math")
  end

  test "should be valid" do
    assert @course.valid?
  end

  test "name should be present" do
    @course.name = "      "
    assert_not @course.valid?
  end

  test "code should be present" do
    @course.code = "           "
    assert_not @course.valid?
  end

  test "description should not be too long" do
    @course.description = "a" * 500
    assert_not @course.valid?
  end

  test "code and name should not be too long" do
    @course.name = "a" * 244 + "@example.com"
    assert_not @course.valid?
    @course.code = "b" * 244
    assert_not @course.valid?
  end

end
