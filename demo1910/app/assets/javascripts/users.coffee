# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

email_validation = (value) ->
    regex = /^([\w.-]+)@([\w.-]+)\.([a-zA-Z.]{2,6})$/i

    if value is ""
        $("#login-email-error").text("can't be blank")
        return false
    else if !(value.match regex)
        $("#login-email-error").text("is invalid email")
        return false
    else
        $("#login-email-error").text("")
        return true

buttonEnable = () ->
    result = email_validation($("#email").val())
    if result is true
        $('#submit-button').prop('disabled', false)
    else
        $('#submit-button').prop('disabled', true)

jQuery ->
    $("#email").change ->
        email_validation(@value)
        buttonEnable()
