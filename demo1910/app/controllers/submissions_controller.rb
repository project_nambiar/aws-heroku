class SubmissionsController < ApplicationController

  before_action :require_user, only: [:edit, :update, :show]
  before_action :require_student, only: [:edit, :update]

  def edit

    @assignment = Assignment.find(params[:assignment_id])

    if params[:id].blank? && params[:id] != ""
      @submission = @assignment.submissions.where(user_id: current_user).first
    else
      @submission = Submission.find(params[:id])
    end

    @submission_state = params[:submission_state]
  end

  def update
    s = current_user.submissions.where("assignment_id == #{params[:assignment_id]}").first
    params[:submission_state] = submission_params[:status] if params[:submission_state].nil?
    s.image = nil if !submission_params[:image]
    s.save
    assignment = Assignment.find(params[:assignment_id])
    if params[:submission_state] && !assignment.khan_exercise.blank?
      case params[:submission_state]
      when "Complete"
        s.status = "Complete"
        s.save
      when "Started"
        s.status = "Started"
        s.save
      when "Not Yet Started"
        s.status = "Not Yet Started"
        s.save
      end
    else
      s.status = "Complete"
      s.save
    end
    s.update_attributes(submission_params)
    redirect_to assignment_path(id: params[:assignment_id])
  end

  def show
    @submission = Submission.find(params[:id])
  end

private

  def submission_params
    params.require(:submission).permit(:status, :image, :comment)
  end

end
