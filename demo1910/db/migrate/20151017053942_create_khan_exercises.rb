class CreateKhanExercises < ActiveRecord::Migration
  def change
    create_table :khan_exercises do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
